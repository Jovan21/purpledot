function toggleText(button) {
  const $textContainer = $(button).parent();
  const $additionalText = $textContainer.find(".additional-text");

  if ($additionalText.is(":hidden")) {
    $additionalText.show();
    $(button).text("Read Less");
  } else {
    $additionalText.hide();
    $(button).text("Read More");
  }
}

// console.log($)

function createChart() {
  $("#chart").kendoChart({
    title: {
      text: "Total Sales",
    },
    subtitle: {
      text: "Year:2022",
    },
    legend: {
      visible: true,
    },
    seriesDefaults: {
      type: "line",
    },

    plotArea: {
      border: {
        width: 2,
        color: "gray",
      },
    },
    series: [
      {
        name: "Asus Laptops",
        data: [20, 63, 45, 98, 112, 19],
        color: "red",
      },
      {
        name: "Hp Laptops",
        data: [52, 31, 29, 90, 130, 165],
        color: "blue",
      },
    ],
    valueAxis: {
      max: 200,
      line: {
        visible: false,
      },
      minorGridLines: {
        visible: true,
      },
      labels: {
        rotation: "auto",
      },
    },
    categoryAxis: {
      categories: ["Jan", "Feb", "Mar", "Apr", "May", "Jun"],
      majorGridLines: {
        visible: false,
      },
    },
    tooltip: {
      visible: true,
      template: "#= series.name #: #= value #",
    },
  });
}

$(document).ready(createChart);
$(document).bind("kendo:skinChange", createChart);

$(document).ready(function () {
  var dataSource = new kendo.data.DataSource({
    data: [
      { CountryID: 1, CountryName: "Macedonia" },
      { CountryID: 2, CountryName: "Serbia" },
      { CountryID: 3, CountryName: "Albania" },
      { CountryID: 4, CountryName: "Greece" },
      { CountryID: 5, CountryName: "Bulgaria" },
      { CountryID: 6, CountryName: "Croatia" },
      { CountryID: 7, CountryName: "Slovenia" },
      { CountryID: 8, CountryName: "Germany" },
      { CountryID: 9, CountryName: "England" },
      { CountryID: 10, CountryName: "Norway" },
      { CountryID: 11, CountryName: "Sweden" },
      { CountryID: 12, CountryName: "Finland" },
      { CountryID: 13, CountryName: "Russia" },
      { CountryID: 14, CountryName: "USA" },
      { CountryID: 15, CountryName: "Spain" },
      { CountryID: 16, CountryName: "Portugal" },
      { CountryID: 17, CountryName: "France" },
      { CountryID: 18, CountryName: "Italy" },
      { CountryID: 19, CountryName: "Mexico" },
      { CountryID: 20, CountryName: "Argentina" },
    ],
    sort: { field: "CountryName", dir: "asc" },
  });

  $("#kd-place-chooser").kendoDropDownList({
    filter: "contains",
    optionLabel: "Please select country...",
    dataTextField: "CountryName",
    dataValueField: "CountryID",
    dataSource: dataSource,
  });
});

$(function () {
  $("#form").submit(function (event) {
    event.preventDefault();

    var name = $("#name").val();
    var email = $("#email").val();
    var country = $("#kd-place-chooser").data("kendoDropDownList").text();
    var message = $("#message").val();

    var isValid = true;

    if (name === "") {
      isValid = false;
    }

    if (email === "") {
      isValid = false;
    }

    if (country === "") {
      isValid = false;
    }

    if (message === "") {
      isValid = false;
    }

    if (isValid) {
      alert("Form submitted successfully");
      $("#form")[0].reset();
    } else {
      alert("Please fill the form correctly");
    }
  });
});

document.addEventListener("DOMContentLoaded", function () {
  var modeToggle = document.getElementById("mode-toggle");
  modeToggle.addEventListener("click", function (event) {
    event.preventDefault();
    toggleMode();
  });

  var preferredMode = localStorage.getItem("preferredMode");
  if (preferredMode === "dark") {
    enableDarkMode();
  } else {
    enableLightMode();
  }

  var navLinks = document.getElementsByClassName("nav-link");
  for (var i = 0; i < navLinks.length; i++) {
    navLinks[i].addEventListener("click", function (event) {
      localStorage.setItem(
        "preferredMode",
        document.body.classList.contains("dark-mode") ? "dark" : "light"
      );
    });
  }
});

function toggleMode() {
  var isDarkMode = document.body.classList.contains("dark-mode");
  if (isDarkMode) {
    enableLightMode();
  } else {
    enableDarkMode();
  }
}

function enableLightMode() {
  document.body.classList.remove("dark-mode");
  localStorage.setItem("preferredMode", "light");
}

function enableDarkMode() {
  document.body.classList.add("dark-mode");
  localStorage.setItem("preferredMode", "dark");
}

$(document).ready(function () {
  $(".buy").click(function () {
    $(".popup").fadeIn();
  });

  $(".popup").click(function () {
    $(this).fadeOut();
  });

  $(".close").click(function () {
    $(".popup").fadeOut();
  });

  $("li").click(function () {
    $(".popup").fadeOut();
  });
});

$(document).ready(function () {
  $("#grid").kendoGrid({
    dataSource: {
      data: [
        {
          OrderID: 1,
          Price: 799,
          ShipName: "Asus",
          OrderDate: new Date("2023-04-01"),
        },
        {
          OrderID: 2,
          Price: 1234,
          Name: "Asus 2",
          OrderDate: new Date("2023-02-01"),
        },
        {
          OrderID: 3,
          Price: 655,
          Name: "Acer",
          OrderDate: new Date("2023-09-08"),
        },
        {
          OrderID: 4,
          Price: 456,
          Name: "Lenovo",
          OrderDate: new Date("2023-07-22"),
        },
        {
          OrderID: 5,
          Price: 1400,
          Name: "Lenovo 2020",
          OrderDate: new Date("2023-04-04"),
        },
        {
          OrderID: 6,
          Price: 999,
          Name: "Hp",
          OrderDate: new Date("2023-12-19"),
        },
        {
          OrderID: 7,
          Price: 977,
          Name: "Hp 2019",
          OrderDate: new Date("2023-07-08"),
        },
        {
          OrderID: 8,
          Price: 3456,
          Name: "acer 2022",
          OrderDate: new Date("2023-07-11"),
        },
        {
          OrderID: 9,
          Price: 231,
          Name: "Apple",
          OrderDate: new Date("2023-07-14"),
        },
        {
          OrderID: 10,
          Price: 626,
          Name: "Macbook",
          OrderDate: new Date("2023-07-30"),
        },
        {
          OrderID: 11,
          Price: 756,
          Name: "Apple 2017",
          OrderDate: new Date("2023-03-02"),
        },
        {
          OrderID: 12,
          Price: 432,
          Name: "Samsung",
          OrderDate: new Date("2023-04-02"),
        },
        {
          OrderID: 13,
          Price: 978,
          Name: "Samsung 2015",
          OrderDate: new Date("2023-05-02"),
        },
        {
          OrderID: 14,
          Price: 567,
          Name: "Asus",
          OrderDate: new Date("2023-07-02"),
        },
        {
          OrderID: 15,
          Price: 1424,
          Name: "Lenovo Think pad",
          OrderDate: new Date("2023-06-22"),
        },
        {
          OrderID: 16,
          Price: 1888,
          Name: "Asus 2019",
          OrderDate: new Date("2023-02-21"),
        },
      ],
      schema: {
        model: {
          fields: {
            OrderID: { type: "number" },
            Price: { type: "number" },
            Name: { type: "string" },
            OrderDate: { type: "date" },
          },
        },
      },
      pageSize: 20,
      serverPaging: true,
      serverFiltering: false,
    },
    height: 550,
    filterable: {
      mode: "row",
    },
    pageable: true,
    columns: [
      {
        field: "OrderID",
        width: 225,
        filterable: {
          cell: {
            showOperators: false,
          },
        },
      },
      {
        field: "Name",
        width: 500,
        title: "Name",
        filterable: {
          cell: {
            operator: "contains",
            suggestionOperator: "contains",
          },
        },
      },
      {
        field: "Price",
        width: 255,
        filterable: {
          cell: {
            operator: "gte",
          },
        },
      },
      {
        field: "OrderDate",
        width: 255,
        title: "Order Date",
        format: "{0:MM/dd/yyyy}",
      },
    ],
  });
});
